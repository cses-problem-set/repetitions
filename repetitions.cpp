#include <iostream>
#include <string>

using namespace std;

string val;
int lr = 1;

void repetitions(string str);

int main()
{
    cin >> val;
    repetitions(val);
}

void repetitions(string str)
{
    int i = -1;
    int max = 1;
    while(str[++i])
    {
        if (str[i] == str[i + 1])
            max++;
        else
            if (max > lr)
            {
                lr = max;
                max = 1;
            }
            else
                max = 1;
    }
    cout << lr;
}